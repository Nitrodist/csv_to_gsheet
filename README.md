# csv_to_gsheet

This is a command line utility that imports into a new Google Sheets document and then opens the resulting Google Sheet document in the user's browser via Mac OS X's `open http://foobar.com/` command.

## Install

```
gem install csv_to_gsheet
```

## Usage

First, you must download the `credentials.json` file from Google's website. Go to [here](https://developers.google.com/identity/sign-in/web/sign-in) and click on 'Configure A Project'. From there, you'll create an OAuth2 application owned by your own account.

That OAuth2 application will then ask for permission to have access to you personal Google Sheets account via a web link that will be shown to you on your first invocation of the `csv_to_gsheet` command.

Store the downloaded `credentials.json` file in `$HOME/.csv_to_gsheet_google_api_credentials.json`. After you invoke the program, another local file with a token authorizing the local csv_to_gsheet command to manipulate your Google Sheets spreadsheets will be created in `$HOME/.csv_to_gsheet_google_oauth2_token.yaml`.

Once you have your `credentials` file renamed to the correct file, run the command:

```
csv_to_gsheet my_file.csv
```

## Video of it working

[![](screencast_poster.png)](http://www.youtube.com/watch?v=vvOoG__E1gk "")

## License

csv_to_gsheet is licensed under LGPL v3.0 or later. If this doesn't work for you, reach out to me and we can work something out.
