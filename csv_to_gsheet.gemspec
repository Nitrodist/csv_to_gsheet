Gem::Specification.new do |s|
  s.name                  = 'csv_to_gsheet'
  s.version               = '0.0.2'
  s.licenses              = ['LGPL-3.0-or-later']
  s.summary               = "Take local CSV files and create Google Sheet spreadsheeets." 
  s.description           = "This is a command line utility that takes a local CSV file, creates a new Google Sheet spreadsheet, and then povides a web link to the newly created Google Sheet spreadsheet."
  s.authors               = ["Mark Campbell"]
  s.email                 = 'me@markcampbell.me'
  s.files                 = `git ls-files -- lib/*`.split("\n")
  s.executables           = `git ls-files -- exe/*`.split("\n").map{ |f| File.basename(f) }
  s.bindir                = 'exe'
  s.require_path          = "lib"
  s.homepage              = 'https://gitlab.com/Nitrodist/csv_to_gsheet'
  s.metadata              = { "source_code_uri" => "https://gitlab.com/Nitrodist/csv_to_gsheet" }
  s.add_runtime_dependency "google-api-client", "~> 0.29"
  s.add_development_dependency "pry", "~> 0.12"
end
