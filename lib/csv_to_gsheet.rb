require 'csv'
require 'google/apis/sheets_v4'
require 'googleauth'
require 'googleauth/stores/file_token_store'
require 'fileutils'

Google::Apis::RequestOptions.default.retries = 5

module CsvToGsheet

  OOB_URI = 'urn:ietf:wg:oauth:2.0:oob'.freeze
  APPLICATION_NAME = 'csv_to_gsheet_managed_by_you'.freeze
  CREDENTIALS_PATH = "#{ENV['HOME']}/.csv_to_gsheet_google_api_credentials.json".freeze

  # The file token.yaml stores the user's access and refresh tokens, and is
  # created automatically when the authorization flow completes for the first
  # time.
  TOKEN_PATH = "#{ENV['HOME']}/.csv_to_gsheet_google_oauth2_token.yaml".freeze
  File.delete(TOKEN_PATH) if File.empty?(TOKEN_PATH)
  SCOPE = Google::Apis::SheetsV4::AUTH_SPREADSHEETS

  Google::Apis.logger.level = ENV['DEBUG'] != nil ? Logger::DEBUG : Logger::FATAL

  module Runner
    def self.invoke
      file_path = ARGV[0]
      file_name = file_path.gsub(/.*\//, '')

      # We clear ARGV here so that the `gets` command later will instead prompt from
      # standard in for the user's code in our `authorize` function
      #
      # See https://stackoverflow.com/questions/2166862/why-is-gets-throwing-an-error-when-arguments-are-passed-to-my-ruby-script
      ARGV.clear

      # Check files to see if they exist and if they don't then tell the user what to do
      #
      # This exits the program early if the credentials.json-equivalent file is not present.
      CsvToGsheet.check_token_files

      # This is a pre-flight check. We verify that the file is correctly
      # formatted by parsing the csv and discarding the result. The way this
      # library has been written so far is to read the entire file into memory
      # anyway, so if we do encounter larger files in the future, we'll change
      # this around as well. In the mean time, I think it's fine to keep
      # prasing it as is.
      CsvToGsheet::CSV.check_parsing_the_csv(file_path)

      # Initialize the API
      service = Google::Apis::SheetsV4::SheetsService.new
      service.client_options.application_name = APPLICATION_NAME
      service.authorization = CsvToGsheet.authorize

      # Read a spreadsheet
      #
      #  # Prints the names and majors of students in a sample spreadsheet:
      #  # https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
      #  spreadsheet_id = '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms'
      #  range = 'Class Data!A2:E'
      #  response = service.get_spreadsheet_values(spreadsheet_id, range)
      #  puts 'Name, Major:'
      #  puts 'No data found.' if response.values.empty?
      #  response.values.each do |row|
      #    # Print columns A and E, which correspond to indices 0 and 4.
      #    puts "#{row[0]}, #{row[4]}"
      #  end

      #  # Find or create a spreadsheet
      #  #
      #  preexisting_spreadsheet_id = "19B3KW7Rt1cr0hq6YcKeug96vb5Qxjw1TaNtC578cdQw"
      #
      #  spreadsheet = begin
      #    service.get_spreadsheet(preexisting_spreadsheet_id)
      #  rescue Google::Apis::ClientError => e
      #    # handle the case where we don't know what exception was raised
      #    raise e if e.status_code != 404
      #
      #    # create the spreadsheet here.
      #    spreadsheet = {
      #      properties: {
      #        title: file_name
      #      }
      #    }
      #    spreadsheet = service.create_spreadsheet(spreadsheet,
      #                                             fields: 'spreadsheetId')
      #  end

      # Create a spreadsheet
      #
      spreadsheet = {
        properties: {
          title: file_name
        }
      }
      spreadsheet = service.create_spreadsheet(spreadsheet,
                                               fields: 'spreadsheetId')
      puts "Spreadsheet ID: #{spreadsheet.spreadsheet_id}"

      # append to table in spreadsheet
      #    _ _
      # A| 1 2
      # B| 3 4
      #
      # =>
      #
      #    - -
      # A| 1 2
      # B| 3 4
      # C| 1 2
      # D| 3 4

      result = begin
        result = service.append_spreadsheet_value(spreadsheet.spreadsheet_id,
                                         'A1',
                                         { values: ::CSV.parse(File.read(file_path), liberal_parsing: true) },
                                         value_input_option: 'USER_ENTERED')
        puts "\n\u001b[42;1mUploaded csv successfully to range #{result.updates.updated_range}!\u001b[0m"

        result
      rescue StandardError => e
        puts "\n\u001b[41;1m. FAIL \u001b[0m"
        puts e.backtrace
        raise e
      end

      url = "https://docs.google.com/spreadsheets/d/#{spreadsheet.spreadsheet_id}/edit#gid=0&range=#{result.updates.updated_range.gsub(/.*!/, '')}"
      puts "Your Google Sheets spreadsheet: #{url}"
      if RUBY_PLATFORM =~ /darwin/
        `open #{url}`
      end
    end
  end

  def self.check_token_files
    if !File.exists? CREDENTIALS_PATH
      puts "No credentials.json equivalent file for your OAuth2 app exists at #{CREDENTIALS_PATH}"
      puts ""
      puts "Download your credentials.json file from https://developers.google.com/identity/sign-in/web/sign-in"
      puts ""
      puts "Move that credentials.json file to #{CREDENTIALS_PATH}"
      puts ""
      puts "Exiting now without connecting to Google's API"
      exit 1
    end
  end

  module CSV
    def self.check_parsing_the_csv(file_path)
      ::CSV.parse(File.read(file_path), liberal_parsing: true)
      nil
    end
  end

  ##
  # Ensure valid credentials, either by restoring from the saved credentials
  # files or intitiating an OAuth2 authorization. If authorization is required,
  # the user's default browser will be launched to approve the request.
  #
  # @return [Google::Auth::UserRefreshCredentials] OAuth2 credentials
  def self.authorize
    client_id = Google::Auth::ClientId.from_file(CREDENTIALS_PATH)
    token_store = Google::Auth::Stores::FileTokenStore.new(file: TOKEN_PATH)
    authorizer = Google::Auth::UserAuthorizer.new(client_id, SCOPE, token_store)
    user_id = 'default'
    credentials = authorizer.get_credentials(user_id)
    if credentials.nil?
      url = authorizer.get_authorization_url(base_url: OOB_URI)
      puts 'Open the following URL in the browser and enter the ' \
           "resulting code after authorization:\n" + url
      code = gets.chomp
      credentials = authorizer.get_and_store_credentials_from_code(
        user_id: user_id, code: code, base_url: OOB_URI
      )
    end
    credentials
  end

end

